//console.log("Hello World!");

/*

	Create a function that will accept first name, last name, age, current job, reason for career shift, future goals, and return a complete sentence introducing one's self bearing the input of the user. You may use an object data type as an argument, and access the properties of an object inside the function to get the actual value of properties passed in the function.

*/

	/*let user = {
		fName:``,
		lName:``,
		age:``,
		currentJob: ``,
		reason: ``,
		futureGoals:``

	};

	user.fName = prompt(`Enter a First Name:`);
	user.lName = prompt(`Enter a Last Name:`);
	user.age = prompt(`Enter a Age:`);
	user.currentJob = prompt(`Enter your Current Job:`);
	user.reason = prompt(`What is your reason for career shift?`);
	user.futureGoals = prompt(`What is your future goal?`);

	function userIntro(info){
		//console.log(info);
		console.log(`Hello I am ${info.fName +` `+ info.lName}, ${info.age} years of age and I'm currently working as a ${info.currentJob}. I have shift my career because I want to ${info.reason}. My future goals is ${info.futureGoals}`)
	}

	userIntro(user);*/

/*
	Create a conditional statement that gives a user some options whether they will subscribe to a monthly subscription based on given benefits as a subscriber (you can choose any product/services for this scenario)

*/

	/*alert(`We will help you choose the Netflix Plan that's right for you!`);
	
	let budget = prompt(`Enter your budget:`);
	
	function userBudget(amnt){
		if (amnt >= 549) {
			alert(`You are able to avail the Netflix Premium!\nMontly Price: PHP 549\nVideo Quality: BEST\nResolution: 4K + HDR\nScreens you can watch on the same time: 4\nDevice you can use to watch: Phone, Tablet, Computer, & TV`);
		}
		else if (amnt >= 459) {
			alert(`You are able to avail the Netflix Premium!\nMontly Price: PHP 459\nVideo Quality: BETTER\nResolution: 1080p\nScreens you can watch on the same time: 2\nDevice you can use to watch: Phone, Tablet, Computer, & TV`);
		}
		else if (amnt >= 369) {
			alert(`You are able to avail the Netflix Premium!\nMontly Price: PHP 369\nVideo Quality: GOOD\nResolution: 480p\nScreens you can watch on the same time: 1\nDevice you can use to watch: Phone, Tablet, Computer, & TV`);
		}
		else if (amnt >= 149) {
			alert(`You are able to avail the Netflix Premium!\nMontly Price: PHP 149\nVideo Quality: GOOD\nResolution: 480p\nScreens you can watch on the same time: 1\nDevice you can use to watch: Phone & Tablet`);
		}
		else{
			alert(`Oppss.. Budget is out of range!`);
			chkBudget();
		}
	}

	userBudget(budget);

	function chkBudget(){
		let budget = prompt(`Enter another budget again:`);
		userBudget(budget);
	}*/

/*Convert the conditional statement with the use of Ternary Operator*/

	/*alert(`We will help you choose the Netflix Plan that's right for you!`);
	
	let budget = prompt(`Enter your budget:`);
	userBudget(budget);
	
	function userBudget(amnt){
		amnt >= 549 ? 
			alert(`You are able to avail the Netflix Premium!\nMontly Price: PHP 549\nVideo Quality: BEST\nResolution: 4K + HDR\nScreens you can watch on the same time: 4\nDevice you can use to watch: Phone, Tablet, Computer, & TV`)
			:
		amnt >= 459 ?
			alert(`You are able to avail the Netflix Premium!\nMontly Price: PHP 459\nVideo Quality: BETTER\nResolution: 1080p\nScreens you can watch on the same time: 2\nDevice you can use to watch: Phone, Tablet, Computer, & TV`)
			:

		amnt >= 369 ?
			alert(`You are able to avail the Netflix Premium!\nMontly Price: PHP 369\nVideo Quality: GOOD\nResolution: 480p\nScreens you can watch on the same time: 1\nDevice you can use to watch: Phone, Tablet, Computer, & TV`)
			:
		amnt >= 149 ?
			alert(`You are able to avail the Netflix Premium!\nMontly Price: PHP 149\nVideo Quality: GOOD\nResolution: 480p\nScreens you can watch on the same time: 1\nDevice you can use to watch: Phone & Tablet`)
			:
			chkBudget();
	}

	function chkBudget(){
		alert(`Oppss.. Budget is out of range!`)
		let budget = prompt(`Enter another budget again:`);
		userBudget(budget);
	}*/

/*

Like on our previous example, using simple Document Object Model (DOM) properties and methods getElementById() and innerHTML, display the text on the browser as a result base on the given input of the user
		
*/

	function color(){
		let color = document.getElementById('color').value;
		let display = document.getElementById('display');
		display.innerHTML = document.getElementById('text').value
		switch(color){
			case `1` : display.style.color = `red`
				break;
			case `2` : display.style.color = `green`
				break;
			case `3` : display.style.color = `blue`
				break;
			case `4` : display.style.color = `yellow`
				break;
			default: display.style.color = `white`
				
		}
	}

/*

Create each variable for a String, and Array. Using for loop, iterate each character/element from the given variable and store every iterated character/element in a new variable. Using Console.log(), display the value of the new variable in the browser console to see if each character/element is stored in the new variable.

*/

	//solution 1
/*	let string = `superfragilisticalidocious`;
	let array = [];

	for(let n = 0; n < string.length; n++ ){
		array[n]=string[n];
	}

	console.log(array);*/

	//solution 2
/*	let string = `superfragilisticalidocious`;
	let array = [``];
	let n = 0;
	do{
		for(let m = 0; m < string.length; m++ ){
			array[n] += string[m];
		}
		console.log(array);
		n++;
	} while (n < array.length);*/


/*

	Create an array of students. Find the following information using array methods and accessors:
	let students = [`Prince`, `Angel`, `Jeremy`, `Angel`,`Allan`,`Mark`, `Roxanne`];
	console.log(`The students array contain the following arraylist:`, students);

	1.) Total size of an array,
	console.log(`The total length of the students array is: ${students.length}`);

	2.) Adding a new element at the beginning of an array 
	console.log(`Adding a new element at the beginning of the array: ${students.unshift(`Kevin`)}`, students);

	3.) Adding a new element at the end of an array
	console.log(`Adding a new element at the end of the array: ${students.push(`Mikhaella`)}`, students);

	4.) Removing an existing element at the beginning of an array 
	console.log(`Removing an existing element at the beginning of an array: ${students.shift()}`, students);

	5.) Removing an existing element at the end of an array
	console.log(`Removing an existing element at the end of an array: ${students.pop()}`, students);

	6.) Find the index number of an element in an array 
	console.log(`Find the index number of "Allan": ${students.indexOf(`Allan`)}`, students);

	7.) If there are two identical existing element in an array, use an array accessor to find the index number of the first instance of that element from two identical element in an array. 
	let found = false;
	for(let x = 0; x < students.length; x++){
		for(let y = 0; y < students.length; y++){
			if( x !== y){
				if(students[x] !== students[y]){
					console.log(`No duplicate elements found!`);
					console.log(students[x], students[y])
				}
				else{
					console.log(`Duplicate elements found!`);
					console.log(`The first instance of the array is found at element no. ${students.indexOf(students[x])}`);
					console.log(`The second instance of the array is found at element no. ${y}`);
					found = true;
					break;
				}
			}
		}

		if(found === true){
			break;
		}
	}


*/

/*

Create an object that contains properties first name, last name, age, address(contains city and country), references(contains at least 3 people with propety name and email address), friends(with at least 3 friends;s first name), admin status to true, and a function property that contains console.log() displaying text about self introduction about name, age. Search of "this" keyword to know where and how to use it.

*/
	
	
	
	
	
	
	


